package org.stonlexx.dwtest.utility;

import com.google.common.primitives.Primitives;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import net.minecraft.server.v1_13_R2.DataWatcherRegistry;
import net.minecraft.server.v1_13_R2.DataWatcherSerializer;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@UtilityClass
public class ProtocolUtil {

    protected final Map<String, DataWatcherSerializer<?>> serializerMap     = new HashMap<>();
    protected final Map<String, String> datawatcherList                     = new HashMap<>();


    @SneakyThrows
    public void setupProtocol() {
        for (Field field : DataWatcherRegistry.class.getFields()) {

            serializerMap.put(field.getName(), (DataWatcherSerializer<?>) field.get(null));

            Type type = ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];

            if (type instanceof Class<?>) {
                datawatcherList.put(((Class<?>) type).getSimpleName(), field.getName());
            }
        }

        datawatcherList.put("UUID", "o");

        datawatcherList.put("BlockPosition", "m");
        datawatcherList.put("BlockData", "h");
        datawatcherList.put("IBlockData", "h");

        datawatcherList.put("IChatBaseComponent", "f");
        datawatcherList.put("ChatComponentText", "f");
    }

    @SuppressWarnings("unchecked")
    public <T> Optional<DataWatcherSerializer<T>> getEntitySerializer(@NonNull T value) {

        if (Optional.class.isAssignableFrom(value.getClass())) {
            Optional optional = ((Optional) value);

            if (optional.isPresent()) {
                Object object = optional.get();

                return Optional.of((DataWatcherSerializer<T>)
                        serializerMap.get( datawatcherList.get(object.getClass().getSimpleName()) ));
            }

        } else {

            return Optional.of((DataWatcherSerializer<T>)
                    serializerMap.get( datawatcherList.get(Primitives.wrap(value.getClass()).getSimpleName()) ));
        }

        return Optional.empty();
    }
}
