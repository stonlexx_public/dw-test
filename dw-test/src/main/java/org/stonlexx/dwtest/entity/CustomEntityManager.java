package org.stonlexx.dwtest.entity;

import com.mojang.datafixers.DataFixUtils;
import com.mojang.datafixers.schemas.Schema;
import com.mojang.datafixers.types.Type;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.minecraft.server.v1_13_R2.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Getter
public final class CustomEntityManager {

    public static final CustomEntityManager INSTANCE = new CustomEntityManager();

    protected final Map<CustomEntity, EntityTypes> customEntityMap = new HashMap<>();


    /**
     * Зарегистрировать кастомный NMS ентити
     *
     * @param customEntity   - тип кастомного ентити
     * @param entityFunction - функция создания ентити как объекта
     */
    @SuppressWarnings("unchecked")
    public void register(@NonNull CustomEntity customEntity,
                         @NonNull Function<World, Entity> entityFunction) {

        Schema schema = DataConverterRegistry.a().getSchema(DataFixUtils.makeKey(1628));
        Map<Object, Type<?>> typeMap = (Map<Object, Type<?>>) schema.findChoiceType(DataConverterTypes.n).types();

        String minecraftEntityName = ("minecraft:").concat(customEntity.getName());
        String minecraftExtendName = ("minecraft:").concat(customEntity.getExtendEntity());

        typeMap.put(minecraftEntityName, typeMap.get(minecraftExtendName));

        customEntityMap.put(customEntity, EntityTypes.a(customEntity.getName(), EntityTypes.a.a(customEntity.getEntityClass(), entityFunction)));
    }

    /**
     * Заспавнить кастомного ентити
     *
     * @param customEntity - тип кастомного ентити
     * @param location     - локация ентити
     */
    public org.bukkit.entity.Entity spawn(@NonNull CustomEntity customEntity,
                                          @NonNull Location location) {

        BlockPosition blockPosition = new BlockPosition(location.getBlockX(), location.getBlockY(), location.getBlockZ());
        WorldServer worldServer = ((CraftWorld) location.getWorld()).getHandle();

        Entity nmsEntity = customEntityMap.get(customEntity)
                .a(worldServer, null, null, null, blockPosition, true, false);

        return nmsEntity == null ? null : nmsEntity.getBukkitEntity();
    }


    @Getter
    @RequiredArgsConstructor
    public enum CustomEntity {

        ANGRY_OCELOT("angry_ocelot", "ocelot", EntityOcelot.class);


        protected final String name;
        protected final String extendEntity;

        protected final Class<? extends Entity> entityClass;
    }
}
