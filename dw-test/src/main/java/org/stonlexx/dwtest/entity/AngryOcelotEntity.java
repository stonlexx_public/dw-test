package org.stonlexx.dwtest.entity;

import lombok.NonNull;
import net.minecraft.server.v1_13_R2.*;

public class AngryOcelotEntity
        extends EntityOcelot {

    public AngryOcelotEntity(@NonNull World world) {
        super(world);
    }

    @Override
    protected void n() {
        this.goalSit = new PathfinderGoalSit(this);

        this.goalSelector.a(5, new PathfinderGoalMoveTowardsRestriction(this, 1.0D));
        this.goalSelector.a(8, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 8.0F));
        this.goalSelector.a(8, new PathfinderGoalRandomLookaround(this));

        l();
    }

    @Override
    protected void l() {
        this.goalSelector.a(2, new PathfinderGoalOcelotAttack(this));
        this.goalSelector.a(6, new PathfinderGoalMoveThroughVillage(this, 1.0D, false));
        this.goalSelector.a(7, new PathfinderGoalRandomStrollLand(this, 1.0D));

        this.targetSelector.a(2, new PathfinderGoalNearestAttackableTarget<>(this, EntityHuman.class, true));
    }


}
