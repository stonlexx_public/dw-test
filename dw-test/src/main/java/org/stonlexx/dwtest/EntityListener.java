package org.stonlexx.dwtest;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_13_R2.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.stonlexx.dwtest.entity.AngryOcelotEntity;
import org.stonlexx.dwtest.entity.CustomEntityManager;
import org.stonlexx.dwtest.utility.ProtocolUtil;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Optional;

@RequiredArgsConstructor
public final class EntityListener
        implements Listener {

    protected final DwTestPlugin dwTestPlugin;

    @EventHandler
    public void onDeath(@NonNull EntityDeathEvent event) {
        LivingEntity livingEntity = event.getEntity();

        if (event.getEntityType() == EntityType.ZOMBIE) {

            Entity entity = CustomEntityManager.INSTANCE.spawn(CustomEntityManager.CustomEntity.ANGRY_OCELOT,
                    livingEntity.getLocation().clone().add(0, 1, 0));

            if (entity != null) {
                entity.setCustomName(RandomStringUtils.randomAlphanumeric(5));
                entity.setCustomNameVisible(true);
            }

            event.getDrops().clear();
            return;
        }


        if (!(((CraftEntity) livingEntity).getHandle() instanceof AngryOcelotEntity)) {
            return;
        }

        Player playerKiller = livingEntity.getKiller();

        if (playerKiller == null) {
            return;
        }

        event.getDrops().clear();
        Item item = livingEntity.getWorld().dropItemNaturally(livingEntity.getLocation(), new ItemStack(Material.LEATHER));

        for (Player player : Bukkit.getOnlinePlayers()) {

            if (!player.getWorld().equals(item.getWorld())) {
                continue;
            }

            sendMetadataPacket(player, item, 2, Optional.of(new ChatComponentText(ChatColor.YELLOW + player.getName())));
            sendMetadataPacket(player, item, 3, true);
        }

        dwTestPlugin.getMysqlConnection().execute(true, "INSERT INTO `Entities` VALUES (?, ?, ?)",
                playerKiller.getName(), livingEntity.getName(), new Timestamp(System.currentTimeMillis()));
    }


    /**
     * Отправить metadata пакет игроку
     *
     * @param player - игрок
     * @param entity - ентити, которому изменяем metadata
     * @param id     - id метадаты ентити
     * @param value  - значение пакета
     */
    protected <T> void sendMetadataPacket(@NonNull Player player, @NonNull Entity entity, int id, @NonNull T value) {
        PacketPlayOutEntityMetadata entityMetadataPacket = new PacketPlayOutEntityMetadata();

        DataWatcherObject<T> dataWatcherObject = ProtocolUtil.getEntitySerializer(value)
                .map(serializer -> serializer.a(id)).orElse(null);

        setField(entityMetadataPacket, "a", entity.getEntityId());
        setField(entityMetadataPacket, "b", Collections.singletonList(new DataWatcher.Item<>(dataWatcherObject, value)));

        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(entityMetadataPacket);
    }

    /**
     * Изменить переменную в объекте
     * при помощи рефлексии
     *
     * @param object    - объект
     * @param fieldName - имя переменной
     * @param value     - новое значение переменной
     */
    @SneakyThrows
    protected void setField(@NonNull Object object,
                            @NonNull String fieldName,
                            @NonNull Object value) {

        Field field = object.getClass().getDeclaredField(fieldName);

        field.setAccessible(true);
        field.set(object, value);

        field.setAccessible(false);
    }
}
