package org.stonlexx.dwtest;

import lombok.Getter;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;
import org.stonlexx.dwtest.entity.AngryOcelotEntity;
import org.stonlexx.dwtest.entity.CustomEntityManager;
import org.stonlexx.dwtest.mysql.MysqlConnectionBuilder;
import org.stonlexx.dwtest.mysql.MysqlDatabaseConnection;
import org.stonlexx.dwtest.utility.ProtocolUtil;

@Getter
public final class DwTestPlugin
        extends JavaPlugin {

    protected MysqlDatabaseConnection mysqlConnection;


    @Override
    public void onEnable() {
        saveDefaultConfig();
        connectToMysql();

        ProtocolUtil.setupProtocol();

        CustomEntityManager.INSTANCE.register(CustomEntityManager.CustomEntity.ANGRY_OCELOT, AngryOcelotEntity::new);
        getServer().getPluginManager().registerEvents(new EntityListener(this), this);
    }

    /**
     * Подключение к базе данных Mysql
     *  с данными, взятыми из конфигурации плагина
     */
    protected void connectToMysql() {
        ConfigurationSection configurationSection = getConfig().getConfigurationSection("mysql");

        if (configurationSection == null) {
            return;
        }

        this.mysqlConnection = MysqlConnectionBuilder.newMysqlBuilder()

                .mysqlHost(configurationSection.getString("host"))
                .mysqlUsername(configurationSection.getString("username"))
                .mysqlPassword(configurationSection.getString("password"))

                .mysqlPort(configurationSection.getInt("port"))
                .buildConnection()

                .createDatabaseScheme(configurationSection.getString("database"), true)
                .createTable("Entities", "`PlayerName` VARCHAR(16) NOT NULL, `EntityName` VARCHAR(5) NOT NULL, `DeathDate` TIMESTAMP");
    }
}
